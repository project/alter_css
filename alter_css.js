// $Id: alter_css.js
	var ALTER_CSS_NEW = -1;	
	
/**
 * @file
 * Functions for getting, saving, and setting style.css files for themes via AJAX.
 */
 
/**
 * Function saves a new style sheet
 */
function alter_css_save(theme, name, data) {
	var alphanum = /^[0-9A-Za-z]+$/; 
	
	if (alphanum.test(name)) {
		if (document.getElementById('edit-styles').value != document.getElementById('edit-name').value
				&& document.getElementById('edit-styles').value != ALTER_CSS_NEW) {
			replace = document.getElementById('edit-styles').value;
		}
		else {
			replace = '';
		}
		if (name != "backup") {
			$.post(alter_css_path+'save', { theme: theme, name: name, data: data, replace: replace },
				function(data) {
					if (data.substr(0,5) != "ERROR") {
						window.location.href = window.location.href;
					}
					else {
						alert(data);
					}
				}
			);
		}
		else { //Check if creating a new file called "backup" or just altering the existing
			if (document.getElementById('edit-styles').value == ALTER_CSS_NEW) {
				alert("'backup' is a reserved name, please use a different name.");
			}
			else {
				alert("This file can't be altered. If you wish to alter the CSS of this file, copy the contents and paste them into the content field after selecting 'New' on the 'Style Sheet' select box.");
			}
		}
	}
	else {
		alert("The style sheet name must consist of only alphanumeric characters.");
	}
}
/**
 * Function gets style sheet data for a specific name
 */
function alter_css_get(theme, name) {
	if (name != ALTER_CSS_NEW) {
		$.ajax({
			type: "GET",
			url: alter_css_path+'get',
			data: 'theme='+theme+'&name='+name,
			success: function(msg){
				if (msg.substr(0,5) != "ERROR") {
					document.getElementById('edit-name').value = name;
					document.getElementById('edit-data').value = msg;
				}
			}
		});
	}
	else {
		document.getElementById('edit-name').value = "";
		document.getElementById('edit-data').value = "";
	}
}
/**
 * Function sets the default style sheet
 */
function alter_css_set(theme, name) {
	if (name == ALTER_CSS_NEW) {
		alert("You must select an existing style sheet to set as default");
	}
	else {
		$.post(alter_css_path+'set', { theme: theme, name: name },
			function(data) {
				if (data.substr(0,5) != "ERROR") {
					window.location.href = window.location.href;
				}
				else {
					alert(data);
				}
			}
		);
	}
}
/**
 * Function removes a specified style sheet
 */
function alter_css_remove(theme, name) {
	if (name == ALTER_CSS_NEW) {
		alert("You must select an existing style sheet to remove.");
	}
	else if (name == "backup") {
		alert("The backup style sheet cannot be removed.");
	}
	else {
		$.post(alter_css_path+'remove', { theme: theme, name: name },
			function(data) {
				if (data.substr(0,5) != "ERROR") {
					window.location.href = window.location.href;
				}
				else {
					alert(data);
				}
			}
		);
	}
}

