
Module: Alter CSS
Author: Shawn Conn <www.shawnconn.com/contact>
Version: 1.0

Description
===========
Enables a user to alter the CSS for any theme through a the Drupal themes 
administration pages with AJAX.

Installation
============
* Copy the 'alter_css' to module dir & enable per usual Drupal modules.

Usage
=====
After enabling the module, each Drupal theme administration page 
(admin/build/themes/settings/<theme name>) will contain a CSS fieldset where 
the CSS can be edited. On first visit to a theme administration page, Alter CSS 
will create a backup CSS file name style_backup.css in the theme's directory.
This file is a backup of the theme's original style.css and will be restored if 
Alter CSS is uninstalled. This file can also be used to restore the original CSS
if for some reason the theme is broken by changes made with Alter CSS.

The fieldset contains a selectbox of different CSS files that you've created. 
Selecting "New" from the Style Sheets field, will let you enter a new style 
sheet name and CSS rules in the Content field. "Backup" will load 
style_backup.css if you want to look at, copy, or view the original style.css. 
After making any changes, "Save CSS file" will save any changes made to 
style_<css_name>.css. You can remove a file with "Remove CSS file". If you wish 
set the selected file as your theme's style.css, use the Set as style.css 
button. Depending how your site handles caching and aggregating CSS files, you 
might need to reload the page and cache in your web browser 
(Ctrl-F5 for IE and FireFox).
